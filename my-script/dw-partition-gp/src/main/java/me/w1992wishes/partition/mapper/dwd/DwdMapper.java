package me.w1992wishes.partition.mapper.dwd;

import me.w1992wishes.partition.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author w1992wishes 2019/3/20 17:17
 */
@Repository
public interface DwdMapper extends BaseMapper {

}
