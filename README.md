# daily-summary

学习新技术过程的demo，也有常用工具总结...

## 目录

* [数据挖掘相关](data-mining/README.md)
    * [聚类算法](data-mining/cluster-algorithm/README.md)
* [kafka 相关](kafka)
    * [消费者简单例子](kafka/kafaka-consumer)
    * [生产者简单例子](kafka/kafka-producer)
    * [kafka同spring整合例子](kafka/kafka-spring-integration)
* [通用代码](my-common)
* [脚本整理](my-script)
    * [greenplum 数据仓库分区](my-script/dw-partition-gp/README.md)
    * [使用 datax 增量同步数据](my-script/etl-sync-datax/README.md)  
* [Spark 相关](spark/README.md)
    * [etl 离线加载数据预处理](spark/etl-offline-preprocess/README.md)
    * [spark 例子](spark/spark-example/README.md)
    * [参考 spark kmeans 实现的分区聚类](spark/spark-partitioning/README.md)

